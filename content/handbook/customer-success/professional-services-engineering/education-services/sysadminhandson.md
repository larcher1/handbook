---
title: "GitLab System Administration Hands-on Lab Overview"
description: "This hands-on guide is designed to walk you through the lab exercises used in the GitLab System Administration course."
---

# GitLab System Administration

> **We are transitioning to the latest version of this course.** You are using v16 if your group URL is `https://gitlab.com/gitlab-learn-labs/...`. If your group URL starts with `https://spt.gitlabtraining.cloud`, please use the v15 instructions.

## GitLab System Administration Labs
| Lab 1- Install GitLab | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab1.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab1) |
| Lab 2- Use GitLab administration commands | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab2.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab2) |
| Lab 3- Configure GitLab runners | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab3.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab3) |
| Lab 4- Backup and restore GitLab | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab4.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab4) |
| Lab 5- Implement sign-up restrictions | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab5.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab5) |
| Lab 6- Manage GitLab logs | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab6.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab6) |
| Lab 7- Configure instance monitoring | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab7.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab7) |
| Lab 8- Troubleshoot GitLab | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab8.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab8) |


## Quick links

Here are some quick links that may be useful when reviewing this hands-on guide.

- [GitLab System Administration course description](https://about.gitlab.com/services/education/admin/)

## Suggestions?

If you’d like to suggest changes to the *GitLab System Administration Hands-on Guide*, please submit them via merge request.


